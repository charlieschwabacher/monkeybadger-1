# Project Goals

- Should be able to be used as an alternate endpoint for the HoneyBadger plugin (via a monkey patch).
- Code should be well organized and simple.
- Key workflows should be fast and user friendly.
- Keep the code lean, avoid adding dependencies, particularly to accomplish something simple.
