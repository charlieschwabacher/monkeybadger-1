class App < MonkeyBadger::App
  
  # use Rack::Auth::Basic, "Restricted Area" do |username, password|
  #   username == 'user' and password == 'pass'
  # end

  add MonkeyBadger::ClassifierService.new

  add MonkeyBadger::GithubService.new(
    user: 'chloeandisabel',
    repo: 'Candi',
    branch: 'master'
  )

  add MonkeyBadger::EmailService.new(
    from: 'errors@monkeybadger.herokuapp.com',
    to: 'monkey-badger@chloeisabel.pagerduty.com'
  )

  start!
end
