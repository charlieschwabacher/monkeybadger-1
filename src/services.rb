module MonkeyBadger
  
  # superclass for third party service integration
  class Service

    # annotaters add information to errors after they come in
    ANNOTATORS = []

    # qualifiers are able to reject errors and prevent them from running triggers
    QUALIFIERS = []

    # triggers run automatically when an error is reported and passes filters
    TRIGGERS = []

    # actions can be called in response to specific errors by a user through the ui
    ACTIONS = []

    # global actions are called by the user through the ui, but do apply apply to specific errors
    GLOBAL_ACTIONS = []

    # pages is an array {route: proc} mapping routes to their handlers
    PAGES = {}

  end

  # filter errors using a bayesian classifier
  class ClassifierService < Service
    STATUSES = ["important", "unimportant"]

    ANNOTATORS = [:classify]
    QUALIFIERS = [:qualify]
    ACTIONS = STATUSES.map{|status| :"mark_as_#{status}"}
    GLOBAL_ACTIONS = [:classify_all, :reset_training]
    
    PAGES = {
      inspect_classifier: -> (service) {
        @total_words = service.classifier.instance_variable_get(:@total_words)
        @categories = service.classifier.instance_variable_get(:@categories)
        @max_counts = @categories.inject({}){|o, kv| o[kv[0]] = kv[1].values.max; o}

        erb :inspect_classifier
      },
      label_errors: -> (service) {
        @count = Data.count
        @page = (params[:page] || 1).to_i
        @page_size = 25
        @page_count = (@count / @page_size).ceil
        @errors = Data.errors @page, @page_size
        @actions = service.class::ACTIONS.map{|a| service.method a}

        erb :label_errors
      }
    }

    def classify(sha, error)
      blob = "#{error["exception"]} #{error["backtrace"]} #{error["environment"]}"
      classification = classifier.classify blob
      Data.update sha, classification: classification
    end

    def qualify(sha, error)
      error["classification"] == "important"
    end

    STATUSES.each do |status|
      self.send :define_method, :"mark_as_#{status}" do |sha, error|
        Data.update sha, {
          "classification" => status,
          "user_classification" => status
        }
        train!
      end
    end

    def classify_all
      Data.errors.each do |sha, error|
        unless error["user_classification"]
          classify sha, error 
        end
      end
    end

    def reset_training
      reset_classifier!

      keys = Data.shas
      Data.redis.pipelined do
        keys.each do |sha|
          Data.redis.hdel sha, ["classification", "user_classification"]
        end
      end
    end

    def classifier
      if @classifier.nil?
        packed = Data.redis.get "classifier"
        unpacked = packed.unpack("m")[0] rescue nil
        if packed && unpacked && packed.size > 2
          @classifier ||= Marshal.load(unpacked)
        else
          @classifier = Classifier::Bayes.new *STATUSES
        end
      else
        @classifier
      end
    end

    def reset_classifier!
      @classifier = Classifier::Bayes.new *STATUSES
      Data.redis.set "classifier", nil
    end

    def train!
      reset_classifier!
      Data.errors.each do |sha, error|
        classification = error["user_classification"]
        
        if classification
          blob = "#{error["exception"]} #{error["backtrace"]} #{error["environment"]}"
          @classifier.send "train_#{classification}", blob
        end
      end

      marshalled = Marshal.dump(classifier)
      packed = [marshalled].pack("m")
      Data.redis.set("classifier", packed)
    end

  end

  # respond to incoming errors by sending an email
  class EmailService < Service
    TRIGGERS = [:notify_by_email!]

    def initialize(opts)
      @to = opts[:to],
      @from = opts[:from]
    end

    def notify_by_email!(sha, error)
      Pony.mail(
        to: @to,
        from: @from,
        subject: "[#{error["environment"]}] #{error["exception"]}",
        body: "http://monkeybadger.herokuapp.com/errors/#{sha}"
      )
    end
  end

  # annotate errors with a link to the file on github
  class GithubService < Service
    ANNOTATORS = [:github_url]
  
    def initialize(opts)
      @user = opts[:user]
      @repo = opts[:repo]
      @branch = opts[:branch]
    end

    def github_url(sha, error)
      path = error["backtrace"].slice(0, error["backtrace"].index("/n")).split(" ")[1][15..-1]
      
      if /^vendor/.match path
        {}
      else
        {"github url" => "https://github.com/#{@user}/#{@repo}/blob/#{@branch}/#{path}"}
      end
    end

  end


  # click to create stories for errors in pivotal tracker
  
  # class PivotalTrackerService < Service
  #   QUALIFIERS = [:storied?]
  #   ACTIONS = [:story!]

  #   def initialize(opts)
  #     @@api_key = opts[:pivotal_tracker_api_key]
  #     @@project_id = opts[:pivotal_tracker_project_id]
  #   end

  #   # create a story in pivotal tracker for the error
  #   def story!(sha)
  #   end

  #   # reject errors that have already been storied in pivotal
  #   def storied?(sha)
  #   end
  # end


  # class TwilioService < Service
  #   TRIGGERS = [:notify_by_sms!]

  #   def notify_by_sms!(sha)
  #   end
  # end


end