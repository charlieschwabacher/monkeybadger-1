module MonkeyBadger
  class App < Sinatra::Application

    @@services = []

    def self.add(service)
      @@services << service
      
      # add routes defined by service
      service.class.const_get(:PAGES).each do |route, handler|
        get "/pages/#{route}" do
          instance_exec service, &handler
        end
      end

      service
    end

    # return Method objects from services
    [:annotators, :qualifiers, :triggers, :actions, :global_actions].each do |method|
      list = -> {
        @@services.map{|s| s.class.const_get(method.upcase).map{|a| s.method a}}.flatten
      }
      self.send :define_singleton_method, method, &list
      self.send :define_method, method, &list
    end

    # return a list of routes defined by services
    def pages
      @@services.map{|s| s.class.const_get('PAGES').keys}.flatten
    end



    # use basic http authentication
    use Rack::Auth::Basic, "Protected Area" do |username, password|
      username == HTTP_AUTH_USERNAME && password == HTTP_AUTH_PASSWORD
    end



    # we need to define routes based on services, so we create
    # them in a method that can be called after they have been attached

    def self.start!

      # register a new error
      # this is the route the honeybadger gem will post to
      post "/v1/notices/" do

        # after posting an error, the honeybadger gem echos our initial
        # response.  we can ignore these requests
        body = request.body.read
        return unless body.length > 0

        # parse error data
        data = JSON.parse body

        # save error in redis
        sha, error = Data.create({
          "exception" => data["error"]["message"],
          "backtrace" => data["error"]["backtrace"].map{|line| line.values.join " "}.join("\n"),
          "url" => data["request"]["url"],
          "controller" => "#{data["request"]["component"]}##{data["request"]["action"]}",
          "params" => data["request"]["params"].to_json,
          "session" => data["request"]["session"].to_json,
          "environment" => data["server"]["environment_name"]
        })

        # add annotation to error
        annotators.each do |method|
          error = Data.update sha, method.call(sha, error)
        end

        # stop here if the error is rejected by any qualifiers
        qualifiers.each do |method|
          return 200 unless method.call sha, error
        end

        # respond to the error by calling triggers
        triggers.each do |method|
          method.call sha, error
        end

        200
      end

      # display a page listing all errors
      get "/" do
        @count = Data.count
        @page = (params[:page] || 1).to_i
        @page_size = 25
        @page_count = (@count / @page_size).ceil
        @errors = Data.errors @page, @page_size
        @actions = self.class.global_actions
        
        erb :index
      end

      # display a page showing error detail
      get "/errors/:sha" do |sha|
        @sha = sha
        @error = Data.get sha
        @actions = self.class.actions
        
        erb :error
      end

      # perform actions on errors
      actions.each do |action|
        post "/errors/:sha/actions/#{action.name}" do |sha|
          error = Data.get sha
          action.call sha, error
          redirect back
        end
      end

      # perform global actions
      global_actions.each do |action|
        post "/actions/#{action.name}" do
          action.call
          redirect back
        end
      end

      # # display a form allowing a user to manually register errors
      # get "/new_error" do
      #   erb :new_error
      # end

    end
  end
end
