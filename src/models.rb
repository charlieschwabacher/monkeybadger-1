module MonkeyBadger

  class Data

    def self.redis
      @@redis ||= Redis.new(
        host: REDIS_HOST,
        port: REDIS_PORT,
        password: REDIS_PASSWORD
      )
    end

    def self.shas
      redis.zrevrange "errors", 0, -1
    end

    def self.count
      redis.zcard "errors"
    end

    def self.errors(page = 1, page_size = 10)
      from = (page - 1) * page_size
      to = page * page_size - 1
      keys = redis.zrevrange "errors", from, to

      values = redis.pipelined do
        keys.each do |key|
          redis.hgetall key
        end
      end

      Hash[keys.zip values]
    end

    def self.get(sha)
      redis.hgetall sha
    end

    def self.canonicalize(blob)
      blob.gsub(/[\d]{4,}/,'')
    end

    def self.create(error)
      blob = "#{error["exception"]} #{error["backtrace"]} #{error["environment"]}"

      sha = Digest::SHA1.hexdigest self.canonicalize(blob)
      time = Time.now

      if redis.zadd "errors", time.to_f, sha
        error["first"] = time.to_s
        redis.hmset sha, *error.flatten
      end

      redis.hincrby sha, "count", 1
      redis.hset sha, "latest", time.to_s

      [sha, error]
    end

    def self.update(sha, annotations)
      if annotations.length > 0
        redis.hmset sha, *annotations.flatten
      end

      redis.hgetall sha
    end

    def self.clear!
      keys = ["errors"] + shas
      redis.del *keys
    end

    def self.import!(path)
      errors = JSON.parse(File.read(path))
      errors.each do |error|
        create({
          "exception" => error[0],
          "backtrace" => error[1],
          "environment" => error[2]
        })
      end
    end

  end

end
