# What is MonkeyBadger?

MonkeyBadger is an error tracking system that uses a Bayesian classifier to send notifications when it classifies an error as urgent.
It can also learn to ignore the noise of inconsequential errors.

# How does it work?

You simply install the Honeybadger gem in your app, generously created by Honeybadger.io.  Then point Honeybadger errors
at your Monkeybadger endpoint.  When an exception occurs, the classifier determines how critical the error is.  Your team
can respond to critical errors immediately, but can review other errors the next day at the office.

# How does it know which errors are critical?

MonkeyBadger uses a Bayesian classifier that you train using your own exception data.  Go to the training screen and classify some exceptions.
After you have classified a hundred or so, the classifier becomes pretty accurate.  This is the same technology used by many spam filters and
dating sites.

# Setting up a client to use MonkeyBadger.

### Gemfile
```ruby
gem 'honeybadger'
```

### config/intializers/monkeybadger.rb
```ruby
Honeybadger.configure do |config|
  config.api_key = 'your-api-key'
  config.host = 'api.yourhost.io'
  config.secure = false

  # if 80 is not your default http port
  # config.port = 80

  # If you want to test in development
  # config.development_environments = []
end
```
